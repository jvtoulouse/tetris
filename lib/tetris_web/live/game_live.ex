defmodule TetrisWeb.GameLive do
  use TetrisWeb, :live_view

  alias Tetris.Tetromino, as: Tetro
  alias Tetris.Game
  alias Tetris.Square

  @device_width 400
  @device_height 600

  @impl true
  def mount(_params, _session, socket) do
    tick()

    {:ok,
     socket
     |> start_game()}
  end

  @impl true
  def render(assigns) do
    ~L"""
      <section class="phx-hero">
        <div phx-window-keydown="keystroke" >
          <%= assigns |> draw_board %>
        </div>
      </section>
    """
  end

  @impl true
  def handle_info(:tick, socket) do
    new_socket = socket |> move(:down)
    {:noreply, new_socket}
  end

  @impl true
  def handle_event("keystroke", %{"key" => "x"}, socket) do
    {:noreply, socket |> rotate(:cw)}
  end

  def handle_event("keystroke", %{"key" => "w"}, socket) do
    {:noreply, socket |> rotate(:ccw)}
  end

  def handle_event("keystroke", %{"key" => "ArrowLeft"}, socket) do
    {:noreply, socket |> move(:left)}
  end

  def handle_event("keystroke", %{"key" => "ArrowRight"}, socket) do
    {:noreply, socket |> move(:right)}
  end

  def handle_event("keystroke", %{"key" => "ArrowDown"}, socket) do
    {:noreply, socket |> move(:down)}
  end

  # PRIVATE
  defp draw_board(assigns) do
    width = @device_width
    height = @device_height

    ~L"""
    <svg width=<%= width %> height=<%= height %> >
      <rect width=<%= width %> height=<%= height %> style="fill:rgb(10,10,110);"/>
      <%= draw_falling_piece(assigns) %>
    </svg>
    """
  end

  defp draw_falling_piece(%{game: game} = assigns) do
    [fi, sec, thi, fou] = game.tetro.squares
    %Square{center: {a, b}, color: color1} = fi
    %Square{center: {c, d}, color: color2} = sec
    %Square{center: {e, f}, color: color3} = thi
    %Square{center: {g, h}, color: color4} = fou

    ~L"""

     <rect
        width="20" height="20"
        x="<%= a  %>"  y="<%= b %>"
        style="fill:<%= color1 %>;" />
     <rect
        width="20" height="20"
        x="<%= c  %>"  y="<%= d %>"
        style="fill:<%= color2 %>;" />
     <rect
        width="20" height="20"
        x="<%= e   %>"  y="<%= f %>"
        style="fill:<%= color3 %>;" />
     <rect
        width="20" height="20"
        x="<%= g %>"  y="<%= h %>"
        style="fill:<%= color4 %>" />

    """
  end

  defp move(%{assigns: %{game: game}} = socket, :down) do
    new_game = game |> Game.move(:down)
    socket |> assign(game: new_game)
  end

  defp move(%{assigns: %{tetro: tetro}} = socket, :left) do
    new_tetro = tetro |> Tetro.move(:left)
    socket |> assign(tetro: new_tetro)
  end

  defp move(%{assigns: %{tetro: tetro}} = socket, :right) do
    new_tetro = tetro |> Tetro.move(:right)
    socket |> assign(tetro: new_tetro)
  end

  def rotate(%{assigns: %{tetro: tetro}} = socket, :cw) do
    new_tetro = tetro |> Tetro.rotate(:cw)
    socket |> assign(tetro: new_tetro)
  end

  def rotate(%{assigns: %{tetro: tetro}} = socket, :ccw) do
    new_tetro = tetro |> Tetro.rotate(:ccw)
    socket |> assign(tetro: new_tetro)
  end

  defp start_game(socket) do
    new_game = Game.start_game()
    socket |> assign(game: new_game)
  end

  defp tick, do: timer(:tick)

  defp timer(message), do: :timer.send_interval(500, message)
end
