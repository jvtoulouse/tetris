defmodule Tetris.Square do
  alias Tetris.Coords
  @size 10

  defstruct color: nil, center: nil

  def new(coords, color) do
    new_square(center: coords, color: color)
  end

  def move(square, :left) do
    %{square | center: Coords.transform(square.center, :x, -2 * @size)}
  end

  def move(square, :right) do
    %{square | center: Coords.transform(square.center, :x, 2 * @size)}
  end

  def move(square, :down) do
    %{square | center: Coords.transform(square.center, :y, 2 * @size)}
  end

  def move_all(squares, direction, 0) do
    squares |> Enum.map(fn square -> move(square, direction) end)
  end

  def move_all(squares, direction, steps) do
    squares
    |> Enum.map(fn square -> move(square, direction) end)
    |> move_all(direction, steps - 1)
  end

  def rotate(squares, :cw) do
    squares |> compute_rotation(&rotate_90/1)
  end

  def rotate(squares, :ccw) do
    squares |> compute_rotation(&rotate_270/1)
  end

  def compute_width(squares) do
    {min, max} =
      squares
      |> Enum.map(& &1.center)
      |> Enum.map(&Coords.x/1)
      |> Enum.min_max()

    max - min + 2 * @size
  end

  # PRIVATE

  defp compute_rotation(squares, fun) do
    current_center = locate_center(squares)

    squares
    |> Enum.map(fn s -> s |> change_origin(current_center) end)
    |> Enum.map(fun)
    |> Enum.map(fn s -> s |> change_origin(Coords.translate_coord({0, 0}, current_center)) end)
  end

  defp change_origin(square, new_origin) do
    new_center = Coords.translate_coord(square.center, new_origin)
    %{square | center: new_center}
  end

  def rotate_90(square) do
    %{square | center: Coords.rotate_cw(square.center)}
  end

  def rotate_270(square) do
    square |> rotate_90() |> rotate_90() |> rotate_90()
  end

  def generate(:i, color) do
    [{@size, @size}, {@size, 3 * @size}, {@size, 5 * @size}, {@size, 7 * @size}]
    |> create(color)
  end

  def generate(:o, color) do
    [{@size, @size}, {@size, 3 * @size}, {3 * @size, @size}, {3 * @size, 3 * @size}]
    |> create(color)
  end

  def generate(:j, color) do
    [{3 * @size, @size}, {3 * @size, 3 * @size}, {3 * @size, 5 * @size}, {@size, 5 * @size}]
    |> create(color)
  end

  def generate(:l, color) do
    [{@size, @size}, {@size, 3 * @size}, {@size, 5 * @size}, {3 * @size, 5 * @size}]
    |> create(color)
  end

  def generate(:s, color) do
    [{@size, 3 * @size}, {3 * @size, 3 * @size}, {3 * @size, @size}, {5 * @size, @size}]
    |> create(color)
  end

  def generate(:z, color) do
    [{@size, @size}, {3 * @size, @size}, {3 * @size, 3 * @size}, {5 * @size, 3 * @size}]
    |> create(color)
  end

  def generate(:t, color) do
    [{@size, @size}, {@size, 3 * @size}, {@size, 5 * @size}, {3 * @size, 3 * @size}]
    |> create(color)
  end

  # PRIVATE
  def locate_center(squares) do
    squares |> Enum.map(fn s -> s.center end) |> Coords.compute_center()
  end

  defp create(coords, color) do
    coords |> Enum.map(fn coord -> new(coord, color) end)
  end

  defp new_square(opts) do
    __struct__(opts)
  end
end
