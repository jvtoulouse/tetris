defmodule Tetris.Tetromino do
  alias Tetris.{Square}

  defstruct squares: [], form: nil, color: nil

  def new_random() do
    new()
    |> Map.replace(:form, random_form())
    |> add_color
    |> add_shape
  end

  def move(tetro, direction, steps \\ 1) do
    new_squares = Square.move_all(tetro.squares, direction, steps)

    %{tetro | squares: new_squares}
  end

  def rotate(tetro, direction) do
    tetro
    |> rotate_squares(direction)
  end

  def find_center(tetro) do
    Square.locate_center(tetro.squares)
  end

  def width(tetro) do
    tetro.squares
    |> Square.compute_width()
  end

  # PRIVATE
  defp rotate_squares(tetro, direction) do
    new_squares = Square.rotate(tetro.squares, direction)

    %{tetro | squares: new_squares}
  end

  defp add_color(tetro) do
    formColors = %{
      :i => "red",
      :o => "yellow",
      :t => "blue",
      :l => "brown",
      :j => "green",
      :s => "purple",
      :z => "aqua"
    }

    {:ok, color} = formColors |> Map.fetch(tetro.form)
    %{tetro | color: color}
  end

  defp new(opts \\ []) do
    __struct__(opts)
  end

  defp random_form() do
    ~w[i o j l s z t]a
    |> Enum.random()
  end

  defp add_shape(tetro) do
    %{tetro | squares: Square.generate(tetro.form, tetro.color)}
  end
end
