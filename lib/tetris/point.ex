defmodule Tetris.Point do
  @scale 20
  def origin do
    {0, 0}
  end

  def move({x, y}, :left), do: {x - @scale, y}

  def move({x, y}, :right), do: {x + @scale, y}

  def move({x, y}, :down), do: {x, y + @scale}

  def move(_, _) do
    raise "You cannot move the point like this."
  end

  def transpose({x, y}), do: {y, x}

  def mirror({x, y}), do: {3 * @scale - x, y}

  def flip({x, y}), do: {x, 3 * @scale - y}
end
