defmodule Tetris.Game do
  alias Tetris.Tetromino, as: Tetro

  @device_width 400
  @device_height 600

  defstruct tetro: nil, score: 0, junkyard: []

  def new do
    new_tetro = Tetro.new_random() |> Tetro.move(:right, 8)
    __struct__(tetro: new_tetro)
  end

  def start_game() do
    new()
  end

  def move(%Tetris.Game{tetro: tetro} = game, :down) do
    new_tetro = tetro |> Tetro.move(:down)
    %{game | tetro: new_tetro}
  end
end
