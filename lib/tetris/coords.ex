defmodule Tetris.Coords do
  @x_max 400
  @y_max 600

  def transform({x, y}, :x, amount), do: {x + amount, y}

  def transform({x, y}, :y, amount), do: {x, y + amount}

  def in_bounds?({x, y}) do
    x >= 0 and x <= @x_max and y >= 0 and y <= @y_max
  end

  def compute_center(coords) do
    {x_min, x_max} =
      coords
      |> Enum.map(&x/1)
      |> Enum.min_max()

    {y_min, y_max} =
      coords
      |> Enum.map(&y/1)
      |> Enum.min_max()

    {div(x_max + x_min, 2), div(y_max + y_min, 2)}
  end

  def translate_coord({x, y}, {x0, y0} = _new_origin) do
    {x - x0, y - y0}
  end

  def x({x, _}), do: x
  def y({_, y}), do: y

  def rotate_cw({x, y}), do: {-y, x}
end
